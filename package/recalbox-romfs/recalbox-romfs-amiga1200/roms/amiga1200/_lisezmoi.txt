## RECALBOX - SYSTEM AMIGA 1200 ##

Placez ici vos roms amiga 1200.

Les roms doivent être:
- Disquettes ADF (*.adf, zippé or 7zippé)
- Disquettes IPF Disk (*.ipf, zippé)
- Packages RP9 d'Amiga Forever (*.rp9)
- Dossiers WHDL (folder, zip ou lha)

Des fichiers UAE (*.uae) peuvent être utilisés pour specifier une configuration particulière pour un jeu.

Ce système nécessite des bios pour fonctionner (Kickstart 3.1).

Consultez la documentation à cette adresse : 
https://github.com/recalbox/recalbox-os/wiki/Amiga-sur-Recalbox-(FR)
